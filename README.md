# Malicious Payload Detector

## What is this project?
This project involves the utilization of the open-source WAF project "ModSecurity" to detect and analyze malicious payloads directed towards the GitLab Nginx server. The project aims to identify the source IP address of the payload and generate a report to report abuse.